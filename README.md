# Pipeline Library Test

Master: [![pipeline status](https://gitlab.com/ponderbear/pipelinelibrary_test/badges/master/pipeline.svg)](https://gitlab.com/ponderbear/pipelinelibrary_test/commits/master)
Stable: [![pipeline status](https://gitlab.com/ponderbear/pipelinelibrary_test/badges/stable/pipeline.svg)](https://gitlab.com/ponderbear/pipelinelibrary_test/commits/stable)
Development: [![pipeline status](https://gitlab.com/ponderbear/pipelinelibrary_test/badges/development/pipeline.svg)](https://gitlab.com/ponderbear/pipelinelibrary_test/commits/development)

Test project for the [Docker image pipeline library](https://gitlab.com/ponderbear/pipelinelibrary)
